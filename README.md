# README #

readme file for the web app "Discover Blog" created in the module Advanced Webtechnologies set09103 at the Napier University Edinburgh
Made by Katharina Endsuleit
30.11.2016

### What is this repository for? ###

* Quick summary of how to use the app

### How do I run the app? ###

1.) Open the Terminal window of your OS.
Windows:
Navigate to start > search "cmd" (in the little box). Press enter.

Mac:
Navigate to Applications > Utilities > Terminal

Ubuntu and Linu:
By default in Ubuntu and Linux Mint the terminal shortcut key is mapped to Ctrl+Alt+T

-----------

2.) Navigate to 'src'. Make sure the library PyCrypto is installed. If not the library can be used after typing in:
>sudo su

>easy_install py_bcrypt-0.4-py2.7-linux-i686.egg

>exit

-----------

3.) Same for the module micawber. If not installed make sure you are in the 'src' folder. In there is a directory called micawber. Install the module by typing in:

>cd micawber

>python setup.py test

>sudo python setup.py install

-----------

4.) Make sure you are in the 'src' folder where the python file 'changeDB.py' is located. Once you are in the right directory type in the command:

> python changeDB.py


This commenad creates the database. deletes all existing tables and creates new ones.

-----------

5.) Make sure you are still in the 'src' folder where the python file 'blog.py' is located. Once you are in the right directory type in the command:

> python blog.py

The console should show the message:
'Running at http://0.0.0.0:5000/'

-----------

6.) Open your browser and browse http://localhost:5000

-----------

7.) Enjoy the web app. Have fun blogging!