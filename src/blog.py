import bcrypt
import sqlite3 as sql
from functools import wraps
from flask import Flask, redirect, render_template, request, session, url_for, flash
import ConfigParser
import logging
from logging.handlers import RotatingFileHandler
from flask import Flask, url_for
from micawber.providers import bootstrap_basic
from micawber.contrib.mcflask import add_oembed_filters

app = Flask(__name__)

oembed_providers = bootstrap_basic()
add_oembed_filters(app, oembed_providers)

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

def check_auth(email, password):
	with sql.connect("blog.db") as con:
		cur = con.cursor()
		valid_pwhash = cur.execute("select password from users where email like '"+ email +"'").fetchone()[0]
		print valid_pwhash;
		if(email != None and valid_pwhash == bcrypt.hashpw(password.encode('utf-8'), valid_pwhash)):
			return True
		return False
	
def requires_login(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        status = session.get('logged_in', False)
        if not status:
            return redirect(url_for('login'))
        return f(*args, **kwargs)
    return decorated
	
@app.route('/')
def root():
	this_route = url_for('.root')
	app.logger.info("Logging a message from " + this_route)
	return render_template('home.html'),200
	
@app.route('/home')
def home():
	return redirect(url_for('.root'))
	
@app.route('/logout/')
def logout():
    session['logged_in'] = False
    return redirect(url_for('.root')),200 
	
@app.route("/login", methods=['GET', 'POST'])
def login():
	if request.method == 'POST':
		user = request.form['email']
		pw = request.form['password']
	
		if check_auth(request.form['email'], request.form['password']):
			session['logged_in'] = True
			
			with sql.connect("blog.db") as con:
				cur = con.cursor()
				session['username'] = cur.execute("select fName from users where email like '"+ user +"'").fetchone()[0]
				
			this_route = url_for('login')
			app.logger.info("Successful login at route: " + this_route)	
			return redirect(url_for('create')),200 
	this_route = url_for('login')
	app.logger.info("Visit at " + this_route + " but no login")		
	return render_template('login.html'),200 
	
@app.route('/create')
@requires_login
def create():
	this_route = url_for('create')
	app.logger.info("Message from route: " + this_route)
	return render_template('create.html'),200 

@app.route('/newpost',methods = ['POST', 'GET'])
@requires_login
def newpost():
	print "we get before if"	
	if request.method == 'POST':
		print "after if"
		try:
			hd = request.form['headline']
			print "Headline" + hd
			ar = request.form['article']
			print "Article" + ar
			ci = request.form['city']
			print "City" + ci
			au = session['username']
			print "Name" + au
			med = request.form['media']
			print "all stored"
			msg = "Variables stored but Error in adding to Database"
			 
			with sql.connect("blog.db") as con:
				cur = con.cursor()

				cur.execute("INSERT INTO posts (headline, article, city, author, media) VALUES (?,?,?,?,?)",(hd,ar,ci,au,med ) )
				con.commit()
				msg = "Record successfully added"
				this_route = url_for('create')
				app.logger.info("New post added from route: " + this_route)
		except:
			con.rollback()
			msg = "error in insert operation"
			this_route = url_for('create')
			app.logger.info("Unsuccessful try to add a new post from route: " + this_route)
		  
		finally:
			return render_template("result.html", msg = msg), 200 
			con.close()

@app.route('/signup')
def signup():
   	this_route = url_for('signup')
	app.logger.info("Signup at route: " + this_route)
	return render_template('signup.html'), 200 

@app.route('/newuser',methods = ['POST', 'GET'])
def newuser():
	if request.method == 'POST':
		try:
			mail = request.form['email']
			fn = request.form['fName']
			ln = request.form['lName']
			pw = request.form['password']
			pw_hash = bcrypt.hashpw(pw, bcrypt.gensalt())
			
			with sql.connect("blog.db") as con:
				cur = con.cursor()

				cur.execute("INSERT INTO users (email, fName, lName, password) VALUES (?,?,?,?)",(mail,fn,ln,pw_hash, ) )
				con.commit()
				msg = "Record successfully added"
				this_route = url_for('newuser')
				app.logger.info("Sucessfull stored new user " + fn + "at route : " + this_route)
		except:
			con.rollback()
			msg = "error in insert operation"
			this_route = url_for('signup')
			app.logger.info("Unsuccessfull try to signup at route: " + this_route)
		  
		finally:
			return render_template("result.html", msg = msg)
			con.close()			
			
			
@app.route('/all')
def all():
	this_route = url_for('all')
	app.logger.info("Message from route: " + this_route)
	con = sql.connect("blog.db")
	con.row_factory = sql.Row
	cur = con.cursor()
	cur.execute("select * from posts")
	rows = cur.fetchall();
	return render_template("all.html",rows = rows),200
   
@app.route('/hamburg')
def hamburg():
	this_route = url_for('hamburg')
	app.logger.info("Message from route: " + this_route)
	con = sql.connect("blog.db")
	con.row_factory = sql.Row
   
	cur = con.cursor()
	cur.execute("select * from posts where city like 'Hamburg'")
   
	rows = cur.fetchall();
	return render_template("hamburg.html",rows = rows), 200

@app.route('/edinburgh')
def edinburgh():
	this_route = url_for('edinburgh')
	app.logger.info("Message from route: " + this_route)
	con = sql.connect("blog.db")
	con.row_factory = sql.Row
   
	cur = con.cursor()
	cur.execute("select * from posts where city like 'Edinburgh'")
   
	rows = cur.fetchall();
	return render_template("edinburgh.html",rows = rows), 200 

   
@app.route('/allusers')
def allusers():
   con = sql.connect("blog.db")
   con.row_factory = sql.Row
   
   cur = con.cursor()
   cur.execute("select * from users")
   
   rows = cur.fetchall();
   return render_template("allusers.html",rows = rows), 200
 
@app.route('/<id>', methods = ['POST', 'GET'])
def detail(id):
	con = sql.connect("blog.db")
	con.row_factory = sql.Row
	cur = con.cursor()
	cur.execute("select * from posts where id=(?)", (id, ))	
	rows = cur.fetchall();
	
	cur.execute("select headline from posts where id=(?)", (id, ))	
	testdata = cur.fetchall();

	if not testdata:
		return render_template("error.html")
	else:
		return render_template("detail.html",rows = rows), 200

   
@app.route('/own')
@requires_login
def own():
	con = sql.connect("blog.db")
	con.row_factory = sql.Row
	
	cur = con.cursor()
	user = session['username']
	cur.execute("SELECT * FROM posts WHERE author=(?)", (user, ))
	rows = cur.fetchall();
	con.commit()
	return render_template("own.html",rows = rows), 200
	
@app.route('/deletepost', methods=['POST','GET'])
def deletepost():
	if request.method == 'POST':
		try:
			deleteId = request.form['deleteId']
			print deleteId + "should be deleted"
			 
			with sql.connect("blog.db") as con:
				cur = con.cursor()
				print "so far"
				cur.execute("DELETE FROM posts WHERE id=(?)", (deleteId, ))
				con.commit()
				print "even more far"
				mesg = "Record deleted"
				
		except:
			con.rollback()
			mesg = "Not possible to delete"
			print "Error";

		finally:
			print mesg
			return render_template("result.html", msg = mesg), 200
			con.close()
			
@app.route('/updatepost', methods=['POST','GET'])
@requires_login
def updatepost():
	updateId = request.form['udateId']
	con = sql.connect("blog.db")
	con.row_factory = sql.Row
	cur = con.cursor()
	cur.execute("select * from posts where id=(?)", (updateId, ))	
	rows = cur.fetchall();
	
	return render_template("updatepost.html",rows = rows), 200
			
@app.route('/change', methods=['POST','GET'])
def change():
	if request.method == 'POST':
		try:
			updateId = request.form['updateId']
			hd = request.form['headline']
			ar = request.form['article']
			ci = request.form['city']
			me = request.form['media']
			print "New city: " + ci
			print updateId + " got updated"
			 
			with sql.connect("blog.db") as con:
				cur = con.cursor()
				cur.execute("UPDATE posts SET headline=(?), article=(?), city =(?), media =(?) WHERE id=(?)", (hd,ar,ci,me,updateId, ))
				con.commit()
				mesg = "Record updated"
				
		except:
			con.rollback()
			mesg = "Not possible to update"
			print "Error";

		finally:
			print mesg
			return render_template("result.html", msg = mesg), 200
			con.close()		

def init(app):
    config = ConfigParser.ConfigParser()
    try:
        config_location = "etc/logging.cfg"
        config.read(config_location)
        
        app.config['DEBUG'] = config.get("config", "debug")
        app.config['ip_address'] = config.get("config", "ip_address")
        app.config['port'] = config.get("config", "port")
        app.config['url'] = config.get("config", "url")

        app.config['log_file'] = config.get("logging", "name")
        app.config['log_location'] = config.get("logging", "location")
        app.config['log_level'] = config.get("logging", "level")
    except:
        print "Could not read configs from: ", config_location


def logs(app):
    log_pathname = app.config['log_location'] + app.config['log_file']
    file_handler = RotatingFileHandler(log_pathname, maxBytes=1024* 1024 * 10 , backupCount=1024)
    file_handler.setLevel( app.config['log_level'] )
    formatter = logging.Formatter("%(levelname)s | %(asctime)s |  %(module)s | %(funcName)s | %(message)s")
    file_handler.setFormatter(formatter)
    app.logger.setLevel( app.config['log_level'] )
    app.logger.addHandler(file_handler)
			
@app.errorhandler(404)
def page_not_found(error):
  return render_template('error.html'), 404

if __name__ == '__main__':
    init(app)
    logs(app)
    app.run(
        host=app.config['ip_address'], 
        port=int(app.config['port']))