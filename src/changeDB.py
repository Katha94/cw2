import sqlite3

conn = sqlite3.connect('blog.db')
print "Opened database successfully";
conn.execute('DROP TABLE IF EXISTS posts')
print "Deleted Table succesfully";
conn.execute('CREATE TABLE posts (id INTEGER PRIMARY KEY AUTOINCREMENT, headline TEXT, article TEXT, city TEXT, author TEXT, media TEXT)')
print "Table created successfully";
conn.execute('INSERT INTO posts (headline, article, city, author, media) VALUES ("First post", "Test", "Hamburg", "Ich", "https://www.youtube.com/watch?v=5gLp8T4Cev0")')
print "instert into the Table";

conn.execute('DROP TABLE IF EXISTS users')
print "Deleted Table users succesfully";
conn.execute('CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, email TEXT, fName TEXT, lName TEXT, password TEXT)')
print "Table users created successfully";

conn.close()
