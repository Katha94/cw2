import unittest
import blog

class TestingTest(unittest.TestCase):
	def test_root(self):
		self.app = blog.app.test_client()
		out = self.app.get('/')
		assert '200 OK' in out.status
		assert 'charset=utf-8' in out.content_type
		assert 'text/html' in out.content_type
	
	#def login(self, username, password):
	#	self.app = blog.app.test_client()
	#	return self.app.post('/login', data=dict(username=username, password=password), follow_redirects=True)
	#
	#def logout(self):
	#	self.app = blog.app.test_client()
	#	return self.app.get('/logout', follow_redirects=True)
	#
	#def test_login_logout(self):
	#	self.app = blog.app.test_client()
	#	rv = self.login('Mama@web.de', 'mama')
	#	assert 'You were logged in' in rv.data
	#	rv = self.logout()
	#	assert 'You were logged out' in rv.data
	#	rv = self.login('Mamaaa@web.de', 'mama')
	#	assert 'Invalid username' in rv.data
	#	rv = self.login('Mama@web.de', 'mamaaa')
	#	assert 'Invalid password' in rv.data

if __name__ == "__main__":
  unittest.main()